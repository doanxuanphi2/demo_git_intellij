package com.example.demo_git_with_intellij;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoGitWithIntellijApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoGitWithIntellijApplication.class, args);
    }

}
